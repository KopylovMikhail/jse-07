package ru.kopylov.tm.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {

    private static final String SALT = "OPI&L%$4kj";

    public static String hash(String text) {
        text = SALT + text + SALT;
        MessageDigest md = null;
        byte[] digest = new byte[0];
        try {
            md = MessageDigest.getInstance("MD5");
            md.reset();
            md.update(text.getBytes());
            digest = md.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        final BigInteger bigInt = new BigInteger(1,digest);
        final String hashtext = bigInt.toString(16);
        return hashtext;
    }
}
