package ru.kopylov.tm.command;

import ru.kopylov.tm.api.service.ServiceLocator;

public abstract class AbstractCommand {

    protected ServiceLocator bootstrap;

    public AbstractCommand(ServiceLocator bootstrap) {
        this.bootstrap = bootstrap;
    }

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();

}
