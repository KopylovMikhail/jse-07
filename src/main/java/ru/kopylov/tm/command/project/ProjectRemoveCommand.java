package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]\n" +
                "ENTER EXISTING PROJECT NAME:");
        try {
            final String projectName = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getProjectService().remove(projectName, currentUserId))
                System.out.println("[PROJECT " + projectName + " REMOVED]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
