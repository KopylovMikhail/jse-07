package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;
import java.util.List;

public final class ProjectTasksListCommand extends AbstractCommand {

    public ProjectTasksListCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-tasks-list";
    }

    @Override
    public String getDescription() {
        return "Project task list.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            final String projectName = bootstrap.getTerminalService().getReadLine();
            if (bootstrap.getProjectService().tasksList(projectName) == null) {
                System.out.println("SUCH A PROJECT DOES NOT HAVE TASKS OR NAME IS EMPTY.\n");
                return;
            }
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            final List<String> taskList = bootstrap.getProjectService().tasksList(projectName, currentUserId);
            System.out.println("TASKS LIST FOR PROJECT " + projectName + ":");
            int count = 1;
            for (final String taskName : taskList) {
                System.out.println(count++ + ". " + taskName);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("\n");
    }

}
