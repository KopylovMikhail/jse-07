package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return  "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]\n" +
                "ENTER NAME:");
        try {
            final String projectName = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getProjectService().persist(projectName, currentUserId))
                System.out.println("[OK]\n");
            else System.out.println("[SUCH A PROJECT EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
