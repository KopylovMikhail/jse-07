package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class ProjectSetTaskCommand extends AbstractCommand {

    public ProjectSetTaskCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-set-task";
    }

    @Override
    public String getDescription() {
        return "Assign a task to a project.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER EXISTING PROJECT NAME:");
        try {
            final String projectName = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER EXISTING TASK NAME:");
            final String taskName = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getProjectService().setTask(projectName, taskName, currentUserId))
                System.out.println("[OK]\n");
            else System.out.println("SUCH A PROJECT/TASK DOES NOT EXIST OR NAME IS EMPTY.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
