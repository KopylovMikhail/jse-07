package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

public final class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        bootstrap.getProjectService().removeAll(currentUserId);
        System.out.println("[ALL PROJECTS REMOVED]\n");
    }

}
