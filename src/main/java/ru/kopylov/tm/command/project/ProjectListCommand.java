package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.User;

import java.util.List;

public final class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        final List<Project> projectList = bootstrap.getProjectService().findAll(currentUserId);
        System.out.println("[PROJECT LIST]");
        int count = 1;
        for (final Project project : projectList) {
            final User projectUser = bootstrap.getUserService().findById(project.getUserId());
            System.out.println(count++ + ". " + project.getName()  + ", created by: "
                    + projectUser.getLogin());
        }
        System.out.print("\n");
    }

}
