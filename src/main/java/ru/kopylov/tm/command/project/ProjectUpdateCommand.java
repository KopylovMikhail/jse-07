package ru.kopylov.tm.command.project;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class ProjectUpdateCommand extends AbstractCommand {

    public ProjectUpdateCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public void execute() {
        final String nameOld, nameNew;
        System.out.println("[PROJECT UPDATE]\n" +
                "ENTER EXISTING PROJECT NAME:");
        try {
            nameOld = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER NEW PROJECT NAME:");
            nameNew = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getProjectService().merge(nameOld, nameNew, currentUserId))
                System.out.println("[PROJECT " + nameOld + " UPDATED TO " + nameNew + "]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
