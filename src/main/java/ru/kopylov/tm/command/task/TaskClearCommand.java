package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

public final class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        bootstrap.getTaskService().removeAll(currentUserId);
        System.out.println("[ALL TASKS REMOVED]\n");
    }

}
