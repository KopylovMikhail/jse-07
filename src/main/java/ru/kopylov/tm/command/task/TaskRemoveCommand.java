package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class TaskRemoveCommand extends AbstractCommand {

    public TaskRemoveCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK REMOVE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            final String taskName = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getTaskService().remove(taskName, currentUserId))
                System.out.println("[TASK " + taskName + " REMOVED]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
