package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\n" +
                "ENTER NAME:");
        try {
            final String taskName = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getTaskService().persist(taskName, currentUserId))
                System.out.println("[OK]\n");
            else System.out.println("[SUCH A TASK EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
