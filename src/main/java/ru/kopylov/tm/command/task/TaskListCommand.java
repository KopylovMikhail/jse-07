package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.entity.User;

import java.util.List;

public final class TaskListCommand extends AbstractCommand {

    public TaskListCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
        final List<Task> taskList = bootstrap.getTaskService().findAll(currentUserId);
        System.out.println("[TASK LIST]");
        int count = 1;
        for (final Task task : taskList) {
            final User taskUser = bootstrap.getUserService().findById(task.getUserId());
            System.out.println(count++ + ". " + task.getName()  + ", created by: " + taskUser.getLogin());
        }
        System.out.print("\n");
    }

}
