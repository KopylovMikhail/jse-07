package ru.kopylov.tm.command.task;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.io.IOException;

public final class TaskUpdateCommand extends AbstractCommand {

    public TaskUpdateCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "Update selected task.";
    }

    @Override
    public void execute() {
        final String nameOld, nameNew;
        System.out.println("[TASK UPDATE]\n" +
                "ENTER EXISTING TASK NAME:");
        try {
            nameOld = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER NEW TASK NAME:");
            nameNew = bootstrap.getTerminalService().getReadLine();
            final String currentUserId = bootstrap.getStateService().getCurrentUser().getId();
            if (bootstrap.getTaskService().merge(nameOld, nameNew, currentUserId))
                System.out.println("[TASK " + nameOld + " UPDATED TO " + nameNew + "]\n");
            else System.out.println("SUCH A TASK DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
