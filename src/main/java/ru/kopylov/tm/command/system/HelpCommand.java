package ru.kopylov.tm.command.system;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

import java.util.Map;

public final class HelpCommand extends AbstractCommand {

    public HelpCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (final Map.Entry<String, AbstractCommand> entry : bootstrap.getStateService().getCommands().entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().getDescription());
        }
        System.out.print("\n");
    }

}
