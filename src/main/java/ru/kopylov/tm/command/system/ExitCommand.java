package ru.kopylov.tm.command.system;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

public final class ExitCommand extends AbstractCommand {

    public ExitCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
