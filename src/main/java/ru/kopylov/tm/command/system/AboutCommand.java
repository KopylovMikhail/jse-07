package ru.kopylov.tm.command.system;

import com.jcabi.manifests.Manifests;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

public final class AboutCommand extends AbstractCommand {

    public AboutCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getDescription() {
        return "Information about application.";
    }

    @Override
    public void execute() {
        final String version = Manifests.read("Implementation-Version");
        final String developer = Manifests.read("Built-By");
        System.out.println("[ABOUT]"
                    + "\nTask manager"
                    + "\nversion: " + version
                    + "\ndeveloper: " + developer);
    }

}
