package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.HashUtil;

import java.io.IOException;

public final class UserRegistrationCommand extends AbstractCommand {

    public UserRegistrationCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "registration";
    }

    @Override
    public String getDescription() {
        return "User registration.";
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRATION]\n" +
                "ENTER LOGIN:");
        try {
            final String login = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER PASSWORD:");
            final String password = bootstrap.getTerminalService().getReadLine();
            final String hashPassword = HashUtil.hash(password);
            final User user = bootstrap.getUserService().persist(login, hashPassword);
            if (user == null) {
                System.out.println("SUCH USER ALREADY EXIST OR LOGIN/PASSWORD IS EMPTY.\n");
                return;
            }
            bootstrap.getStateService().setCurrentUser(user);
            System.out.println("[REGISTRATION SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
