package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.HashUtil;

import java.io.IOException;

public final class UserUpdateCommand extends AbstractCommand {

    public UserUpdateCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-update";
    }

    @Override
    public String getDescription() {
        return "Update user password.";
    }

    @Override
    public void execute() {
        System.out.println("[USER PASSWORD UPDATE]\n" +
                "ENTER NEW PASSWORD:");
        try {
            final String newPassword = bootstrap.getTerminalService().getReadLine();
            final String newHashPassword = HashUtil.hash(newPassword);
            final User currentUser = bootstrap.getStateService().getCurrentUser();
            if (bootstrap.getUserService().merge(currentUser, newHashPassword) == null) {
                System.out.println("PASSWORD IS EMPTY.\n");
                return;
            }
            final User updatedUser = bootstrap.getUserService().merge(currentUser, newHashPassword);
            bootstrap.getStateService().setCurrentUser(updatedUser);
            System.out.println("[PASSWORD UPDATE SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
