package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;

import java.io.IOException;

public final class UserUpdateLoginCommand extends AbstractCommand {

    public UserUpdateLoginCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "user-update-login";
    }

    @Override
    public String getDescription() {
        return "Update user login.";
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGIN UPDATE]\n" +
                "ENTER NEW LOGIN:");
        try {
            final String newLogin = bootstrap.getTerminalService().getReadLine();
            final User currentUser = bootstrap.getStateService().getCurrentUser();
            if (bootstrap.getUserService().updateLogin(currentUser, newLogin) == null) {
                System.out.println("LOGIN IS EMPTY.\n");
                return;
            }
            bootstrap.getUserService().updateLogin(currentUser, newLogin);
            System.out.println("[LOGIN UPDATE SUCCESS]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
