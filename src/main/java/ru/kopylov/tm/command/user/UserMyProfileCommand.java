package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;

public final class UserMyProfileCommand extends AbstractCommand {

    public UserMyProfileCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "my-profile";
    }

    @Override
    public String getDescription() {
        return "Show current user profile.";
    }

    @Override
    public void execute() {
        if (bootstrap.getStateService().getCurrentUser() == null || bootstrap.getStateService().getCurrentUser().getRole() == null) {
            System.out.println("YOU NEED LOGIN.\n");
            return;
        }
        System.out.println("[USER PROFILE]\n" +
                "LOGIN: " + bootstrap.getStateService().getCurrentUser().getLogin() + "\n" +
                "ROLE: " + bootstrap.getStateService().getCurrentUser().getRole().getDisplayName() + "\n");
    }

}
