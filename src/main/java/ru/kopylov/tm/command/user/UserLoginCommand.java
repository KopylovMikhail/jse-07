package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.util.HashUtil;

import java.io.IOException;

public final class UserLoginCommand extends AbstractCommand {

    public UserLoginCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() {
        System.out.println("[USER AUTHORIZATION]\n" +
                "ENTER LOGIN:");
        try {
            final String login = bootstrap.getTerminalService().getReadLine();
            System.out.println("ENTER PASSWORD:");
            final String password = bootstrap.getTerminalService().getReadLine();
            final String hashPassword = HashUtil.hash(password);
            if (bootstrap.getUserService().findOne(login, hashPassword) == null) {
                System.out.println("SUCH USER DOES NOT EXIST OR LOGIN/PASSWORD IS INCORRECT.\n");
                return;
            }
            final User loggedUser = bootstrap.getUserService().findOne(login, hashPassword);
            bootstrap.getStateService().setCurrentUser(loggedUser);
            System.out.println("[AUTHORIZATION SUCCESS]\n" +
                    "HELLO, " + login + "!\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
