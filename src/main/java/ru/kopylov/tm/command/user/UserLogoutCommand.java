package ru.kopylov.tm.command.user;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.api.service.ServiceLocator;
import ru.kopylov.tm.entity.User;

public final class UserLogoutCommand extends AbstractCommand {

    public UserLogoutCommand(ServiceLocator bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "logout";
    }

    @Override
    public String getDescription() {
        return "Complete user session.";
    }

    @Override
    public void execute() {
        bootstrap.getStateService().setCurrentUser(new User());
        System.out.println("[LOGOUT SUCCESS]\n");
    }

}
