package ru.kopylov.tm.context;

import ru.kopylov.tm.api.repository.*;
import ru.kopylov.tm.api.service.*;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.command.project.*;
import ru.kopylov.tm.command.system.AboutCommand;
import ru.kopylov.tm.command.system.ExitCommand;
import ru.kopylov.tm.command.system.HelpCommand;
import ru.kopylov.tm.command.task.*;
import ru.kopylov.tm.command.user.*;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;
import ru.kopylov.tm.repository.*;
import ru.kopylov.tm.service.*;

import java.io.IOException;

public final class Bootstrap implements ServiceLocator {

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskOwnerRepository taskOwnerRepository = new TaskOwnerRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final IStateRepository stateRepository = new StateRepository();

    private final IProjectService projectService = new ProjectService(projectRepository, taskRepository, taskOwnerRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final IStateService stateService = new StateService(stateRepository);

    private final ITerminalService terminalService = new TerminalService();

    public IProjectService getProjectService() {
        return projectService;
    }

    public ITaskService getTaskService() {
        return taskService;
    }

    public IUserService getUserService() {
        return userService;
    }

    public IStateService getStateService() {
        return stateService;
    }

    public ITerminalService getTerminalService() {
        return terminalService;
    }

    public void init() {
        {
            stateService.registry(new HelpCommand(this));
            stateService.registry(new ProjectCreateCommand(this));
            stateService.registry(new ProjectListCommand(this));
            stateService.registry(new ProjectUpdateCommand(this));
            stateService.registry(new ProjectRemoveCommand(this));
            stateService.registry(new ProjectClearCommand(this));
            stateService.registry(new ProjectSetTaskCommand(this));
            stateService.registry(new ProjectTasksListCommand(this));
            stateService.registry(new TaskCreateCommand(this));
            stateService.registry(new TaskListCommand(this));
            stateService.registry(new TaskUpdateCommand(this));
            stateService.registry(new TaskRemoveCommand(this));
            stateService.registry(new TaskClearCommand(this));
            stateService.registry(new UserLoginCommand(this));
            stateService.registry(new UserLogoutCommand(this));
            stateService.registry(new UserMyProfileCommand(this));
            stateService.registry(new UserRegistrationCommand(this));
            stateService.registry(new UserUpdateCommand(this));
            stateService.registry(new UserUpdateLoginCommand(this));
            stateService.registry(new AboutCommand(this));
            stateService.registry(new ExitCommand(this));
        }
        userInit();
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        do {
            try {
                command = terminalService.getReadLine();
                execute(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!"exit".equals(command));
    }

    private void execute(final String command) {
        if (command == null || command.isEmpty()) return;
        if (checkPermission(command)) {
            System.out.println("You don't have enough permissions.\n");
            return;
        }
        final AbstractCommand abstractCommand = stateService.getCommands().get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

    private boolean checkPermission(final String command) {
        final boolean contentProject = command.contains("project");
        final boolean contentTask = command.contains("task");
        final boolean contentUser = command.contains("user");
        final boolean userNotAuth = (stateService.getCurrentUser().getRole() == null);
        return  ((contentProject || contentTask || contentUser) & userNotAuth);
    }

    private void userInit() {
        final User admin = new User();
        admin.setLogin("admin");
        admin.setRole(TypeRole.ADMIN);
        final User user = new User();
        user.setLogin("user");
        user.setRole(TypeRole.USER);
        userService.merge(admin, "111111");
        userService.merge(user, "222222");
    }

}
