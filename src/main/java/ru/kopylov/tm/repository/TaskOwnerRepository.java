package ru.kopylov.tm.repository;

import ru.kopylov.tm.api.repository.ITaskOwnerRepository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class TaskOwnerRepository implements ITaskOwnerRepository {

    private final static Map<String, List<String>> tasksOwner = new LinkedHashMap<>();

    public void merge(final String projectId, final String taskId) {
        if (tasksOwner.containsKey(projectId)) tasksOwner.get(projectId).add(taskId);
        else {
            final List<String> taskList = new ArrayList<>();
            taskList.add(taskId);
            tasksOwner.put(projectId, taskList);
        }
    }

    public List<String> findAllByProjectId(final String projectId) {
        return tasksOwner.get(projectId);
    }

    public List<String> findAll() { //возвращает список задач, принадлежащих всем проектам
        final List<String> allTaskIdList = new ArrayList<>();
        for (final Map.Entry<String, List<String>> entry : tasksOwner.entrySet()) {
            allTaskIdList.addAll(entry.getValue());
        }
        return allTaskIdList;
    }

}
