package ru.kopylov.tm.repository;

import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.entity.Project;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class ProjectRepository extends AbstractRepository implements IProjectRepository {

    private final static Map<String, Project> projectMap = new LinkedHashMap<>();

    public void merge(final Project project) {
        projectMap.put(project.getId(), project);
    }

    public Project persist(final Project project) {
        return projectMap.putIfAbsent(project.getId(), project);
    }

    public List<Project> findAll() {
        return new ArrayList<>(projectMap.values());
    }

    public List<Project> findAll(final String currentUserId) {
        final List<Project> projects = new ArrayList<>();
        for (final Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (currentUserId.equals(entry.getValue().getUserId()))
                projects.add(entry.getValue());
        }
        return projects;
    }

    public boolean remove(final String projectName) {
        return projectMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(projectName));
    }

    public boolean remove(final String projectName, final String currentUserId) {
        return projectMap.entrySet()
                .removeIf(entry -> (entry.getValue().getName().equals(projectName)
                        & entry.getValue().getUserId().equals(currentUserId)));
    }

    public void removeAll() {
        projectMap.clear();
    }

    public void removeAll(final String currentUserId) {
        projectMap.entrySet()
                .removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    public Project findOne(final String projectName) {
        for (final Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectName.equals(entry.getValue().getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public Project findOne(final String projectName, final String currentUserId) {
        for (final Map.Entry<String, Project> entry : projectMap.entrySet()) {
            if (projectName.equals(entry.getValue().getName()) & currentUserId.equals(entry.getValue().getUserId())) {
                return entry.getValue();
            }
        }
        return null;
    }

}
