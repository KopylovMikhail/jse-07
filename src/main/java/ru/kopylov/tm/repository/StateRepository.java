package ru.kopylov.tm.repository;

import ru.kopylov.tm.api.repository.IStateRepository;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.LinkedHashMap;
import java.util.Map;

public final class StateRepository implements IStateRepository {

    private User currentUser = new User();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public User getCurrentUser() {
        return this.currentUser;
    }

    public void setCurrentUser(final User currentUser) {
        this.currentUser = currentUser;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    public void registry(final AbstractCommand command) {
        commands.put(command.getName(), command);
    }

}
