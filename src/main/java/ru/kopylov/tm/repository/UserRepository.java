package ru.kopylov.tm.repository;

import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.enumerated.TypeRole;

import java.util.LinkedHashMap;
import java.util.Map;

public final class UserRepository extends AbstractRepository implements IUserRepository {

    private final static Map<String, User> userMap = new LinkedHashMap<>();

    public void merge(final User user) {
        userMap.put(user.getId(), user);
    }

    public User persist(final String login, final String password) {
        for (final Map.Entry<String, User> entry : userMap.entrySet()) {
            if (login.equals(entry.getValue().getLogin())) return null;
        }
        final User user = new User();
        user.setLogin(login);
        user.setPassword(password);
        user.setRole(TypeRole.USER);
        userMap.put(user.getId(), user);
        return user;
    }

    public User findOne(final String login, final String password) {
        for (final Map.Entry<String, User> entry : userMap.entrySet()) {
            if (login.equals(entry.getValue().getLogin())) {
                if (password.equals(entry.getValue().getPassword()))
                    return entry.getValue();
            }
        }
        return null;
    }

    public User findById(final String id) {
        return userMap.get(id);
    }

    public boolean remove(final String login) {
        return userMap.entrySet()
                .removeIf(entry -> entry.getValue().getLogin().equals(login));
    }

}
