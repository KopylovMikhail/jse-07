package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractRepository<T extends AbstractEntity> {

    public Map<String, T> entityMap = new LinkedHashMap<>();

    public void merge(T entity) {
        entityMap.put(entity.getId(), entity);
    }

    public List<T> findAll() {
        return new ArrayList<>(entityMap.values());
    }

    public abstract boolean remove(String entityName);

    public void removeAll() {
        entityMap.clear();
    }

}
