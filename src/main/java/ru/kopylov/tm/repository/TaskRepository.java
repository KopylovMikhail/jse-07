package ru.kopylov.tm.repository;

import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class TaskRepository extends AbstractRepository implements ITaskRepository {

    private final static Map<String, Task> taskMap = new LinkedHashMap<>();

    public void merge(final Task task) {
        taskMap.put(task.getId(), task);
    }

    public Task persist(final Task task) {
        return taskMap.putIfAbsent(task.getId(), task);
    }

    public List<Task> findAll() {
        return new ArrayList<>(taskMap.values());
    }

    public List<Task> findAll(final String currentUserId) {
        final List<Task> tasks = new ArrayList<>();
        for (final Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (currentUserId.equals(entry.getValue().getUserId()))
                tasks.add(entry.getValue());
        }
        return tasks;
    }

    public boolean remove(final String taskName) {
        return taskMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(taskName));
    }

    public boolean remove(final String taskName, final String currentUserId) {
        return taskMap.entrySet()
                .removeIf(entry -> (entry.getValue().getName().equals(taskName)
                        & entry.getValue().getUserId().equals(currentUserId)));
    }

    public void removeById(final String taskId) {
        taskMap.remove(taskId);
    }

    public void removeAll() {
        taskMap.clear();
    }

    public void removeAll(final String currentUserId) {
        taskMap.entrySet()
                .removeIf(entry -> entry.getValue().getUserId().equals(currentUserId));
    }

    public Task findOne(final String taskName) {
        for (final Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public Task findOne(final String taskName, final String currentUserId) {
        for (final Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (taskName.equals(entry.getValue().getName()) & currentUserId.equals(entry.getValue().getUserId())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public List<Task> findAllById(final List<String> taskIdList) {
        final List<Task> taskNameList = new ArrayList<>();
        for (final String taskId : taskIdList) {
            taskNameList.add(TaskRepository.taskMap.get(taskId));
        }
        return taskNameList;
    }

}
