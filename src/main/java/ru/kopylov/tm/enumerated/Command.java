package ru.kopylov.tm.enumerated;

public enum Command {

    PROJECT_CREATE,
    PROJECT_LIST,
    PROJECT_UPDATE,
    PROJECT_REMOVE,
    PROJECT_CLEAR,
    PROJECT_SET_TASK,
    PROJECT_TASKS_LIST,
    TASK_CREATE,
    TASK_LIST,
    TASK_UPDATE,
    TASK_REMOVE,
    TASK_CLEAR,
    HELP,
    EXIT,
    UNKNOWN_COMMAND;

}
