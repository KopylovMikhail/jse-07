package ru.kopylov.tm.api.service;

import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    boolean persist(String taskName, String userId);

    List<Task> findAll();

    List<Task> findAll(String currentUserId);

    boolean merge(String nameOld, String nameNew);

    boolean merge(String nameOld, String nameNew, String currentUserId);

    boolean remove(String taskName);

    boolean remove(String taskName, String currentUserId);

    void removeAll();

    void removeAll(String currentUserId);

}
