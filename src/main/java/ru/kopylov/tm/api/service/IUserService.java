package ru.kopylov.tm.api.service;

import ru.kopylov.tm.entity.User;

public interface IUserService {

    User merge(User user, String newPassword);

    User updateLogin(User user, String newLogin);

    User persist(String login, String password);

    User findOne(String login, String password);

    User findById(String id);

}
