package ru.kopylov.tm.api.service;

public interface ServiceLocator {

    IProjectService getProjectService();

    ITaskService getTaskService();

    IUserService getUserService();

    IStateService getStateService();

    ITerminalService getTerminalService();

}
