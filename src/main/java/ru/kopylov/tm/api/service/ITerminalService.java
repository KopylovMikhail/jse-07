package ru.kopylov.tm.api.service;

import java.io.IOException;

public interface ITerminalService {

    String getReadLine() throws IOException;

}
