package ru.kopylov.tm.api.service;

import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    boolean persist(String projectName, String userId);

    List<Project> findAll();

    List<Project> findAll(String currentUserId);

    boolean merge(String nameOld, String nameNew);

    boolean merge(String nameOld, String nameNew, String currentUserId);

    boolean remove(String projectName);

    boolean remove(String projectName, String currentUserId);

    void removeAll();

    void removeAll(String currentUserId);

    boolean setTask(String projectName, String taskName);

    boolean setTask(String projectName, String taskName, String currentUserId);

    List<String> tasksList(String projectName);

    List<String> tasksList(String projectName, String currentUserId);

}
