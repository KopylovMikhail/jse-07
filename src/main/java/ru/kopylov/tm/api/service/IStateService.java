package ru.kopylov.tm.api.service;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.Map;

public interface IStateService {

    User getCurrentUser();

    void setCurrentUser(User currentUser);

    Map<String, AbstractCommand> getCommands();

    void registry(AbstractCommand command);

}
