package ru.kopylov.tm.api.repository;

import java.util.List;

public interface ITaskOwnerRepository {

    void merge(String projectId, String taskId);

    List<String> findAllByProjectId(String projectId);

    List<String> findAll();

}
