package ru.kopylov.tm.api.repository;

import ru.kopylov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void merge(Project project);

    Project persist(Project project);

    List<Project> findAll();

    List<Project> findAll(String currentUserId);

    boolean remove(String projectName);

    boolean remove(String projectName, String currentUserId);

    void removeAll();

    void removeAll(String currentUserId);

    Project findOne(String projectName);

    Project findOne(String projectName, String currentUserId);

}
