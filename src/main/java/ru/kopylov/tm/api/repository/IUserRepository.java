package ru.kopylov.tm.api.repository;

import ru.kopylov.tm.entity.User;

public interface IUserRepository {

    void merge(User user);

    User persist(String login, String password);

    User findOne(String login, String password);

    User findById(String id);

    boolean remove(String login);

}
