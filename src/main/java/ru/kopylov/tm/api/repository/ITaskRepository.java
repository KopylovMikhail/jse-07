package ru.kopylov.tm.api.repository;

import ru.kopylov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void merge(Task task);

    Task persist(Task task);

    List<Task> findAll();

    List<Task> findAll(String currentUserId);

    boolean remove(String taskName);

    boolean remove(String taskName, String currentUserId);

    void removeById(String taskId);

    void removeAll();

    void removeAll(String currentUserId);

    Task findOne(String taskName);

    Task findOne(String taskName, String currentUserId);

    List<Task> findAllById(List<String> taskIdList);

}
