package ru.kopylov.tm.api.repository;

import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.Map;

public interface IStateRepository {

    User getCurrentUser();

    void setCurrentUser(User currentUser);

    Map<String, AbstractCommand> getCommands();

    void registry(AbstractCommand command);

}
