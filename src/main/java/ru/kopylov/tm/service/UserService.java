package ru.kopylov.tm.service;

import ru.kopylov.tm.api.service.IUserService;
import ru.kopylov.tm.entity.User;
import ru.kopylov.tm.api.repository.IUserRepository;
import ru.kopylov.tm.util.HashUtil;

public final class UserService extends AbstractService implements IUserService {

    private IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User merge(final User user, final String newPassword) {
        if (newPassword == null || newPassword.isEmpty()) return null;
        user.setPassword(newPassword);
        userRepository.merge(user);
        return user;
    }

    public User updateLogin(final User user, final String newLogin) {
        if (newLogin == null || newLogin.isEmpty()) return null;
        user.setLogin(newLogin);
        userRepository.merge(user);
        return user;
    }

    public User persist(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.persist(login, password);
    }

    public User findOne(final String login, final String password) {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        return userRepository.findOne(login, password);
    }

    public User findById(final String id) {
        if (id == null || id.isEmpty()) return null;
        return userRepository.findById(id);
    }
}
