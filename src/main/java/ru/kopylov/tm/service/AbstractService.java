package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.AbstractEntity;
import ru.kopylov.tm.repository.AbstractRepository;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> {

    protected AbstractRepository<T> abstractRepository;

    public List<T> findAll() {
        return abstractRepository.findAll();
    }
}
