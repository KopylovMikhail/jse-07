package ru.kopylov.tm.service;

import ru.kopylov.tm.api.repository.IStateRepository;
import ru.kopylov.tm.api.service.IStateService;
import ru.kopylov.tm.command.AbstractCommand;
import ru.kopylov.tm.entity.User;

import java.util.Map;

public final class StateService implements IStateService {

    private IStateRepository stateRepository;

    public StateService(final IStateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    public User getCurrentUser() {
        return stateRepository.getCurrentUser();
    }

    public void setCurrentUser(final User currentUser) {
        stateRepository.setCurrentUser(currentUser);
    }

    public Map<String, AbstractCommand> getCommands() {
        return stateRepository.getCommands();
    }

    public void registry(final AbstractCommand command) {
        stateRepository.registry(command);
    }

}
