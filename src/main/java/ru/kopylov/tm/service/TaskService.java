package ru.kopylov.tm.service;

import ru.kopylov.tm.api.service.ITaskService;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.api.repository.ITaskRepository;

import java.util.List;

public final class TaskService extends AbstractService implements ITaskService {

    private ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public boolean persist(final String taskName, final String userId) {
        if (taskName == null || taskName.isEmpty()) return false;
        final Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        return !task.equals(taskRepository.persist(task));
    }

    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    public List<Task> findAll(final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        return taskRepository.findAll(currentUserId);
    }

    public boolean merge(final String nameOld, final String nameNew) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (taskRepository.findOne(nameOld) == null) return false;
        final Task task = taskRepository.findOne(nameOld);
        task.setName(nameNew);
        taskRepository.merge(task);
        return true;
    }

    public boolean merge(final String nameOld, final String nameNew, final String currentUserId) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (taskRepository.findOne(nameOld, currentUserId) == null) return false;
        final Task task = taskRepository.findOne(nameOld, currentUserId);
        task.setName(nameNew);
        taskRepository.merge(task);
        return true;
    }

    public boolean remove(final String taskName) {
        if (taskName == null || taskName.isEmpty()) return false;
        return taskRepository.remove(taskName);
    }

    public boolean remove(final String taskName, final String currentUserId) {
        if (taskName == null || taskName.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        return taskRepository.remove(taskName, currentUserId);
    }

    public void removeAll() {
        taskRepository.removeAll();
    }

    public void removeAll(final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        taskRepository.removeAll(currentUserId);
    }

}
