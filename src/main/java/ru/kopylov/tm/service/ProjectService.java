package ru.kopylov.tm.service;

import ru.kopylov.tm.api.repository.IProjectRepository;
import ru.kopylov.tm.api.repository.ITaskOwnerRepository;
import ru.kopylov.tm.api.repository.ITaskRepository;
import ru.kopylov.tm.api.service.IProjectService;
import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    private IProjectRepository projectRepository;

    private ITaskRepository taskRepository;

    private ITaskOwnerRepository taskOwnerRepository;

    public ProjectService(
            final IProjectRepository projectRepository,
            final ITaskRepository taskRepository,
            final ITaskOwnerRepository taskOwnerRepository
    ) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.taskOwnerRepository = taskOwnerRepository;
    }

    public boolean persist(final String projectName, final String userId) {
        if (projectName == null || projectName.isEmpty()) return false;
        final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        return !project.equals(projectRepository.persist(project));
    }

    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    public List<Project> findAll(final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        return projectRepository.findAll(currentUserId);
    }

    public boolean merge(final String nameOld, final String nameNew) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (projectRepository.findOne(nameOld) == null) return false;
        final Project project = projectRepository.findOne(nameOld);
        project.setName(nameNew);
        projectRepository.merge(project);
        return true;
    }

    public boolean merge(final String nameOld, final String nameNew, final String currentUserId) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (projectRepository.findOne(nameOld, currentUserId) == null) return false;
        final Project project = projectRepository.findOne(nameOld, currentUserId);
        project.setName(nameNew);
        projectRepository.merge(project);
        return true;
    }

    public boolean remove(final String projectName) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (projectRepository.findOne(projectName) == null) return false;
        final String projectId = projectRepository.findOne(projectName).getId();
        final List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskIdList != null) {
            for (final String taskId : taskIdList) {
                taskRepository.removeById(taskId); //вместе с удалением проекта удаляем все задачи проекта
            }
        }
        return projectRepository.remove(projectName);
    }

    public boolean remove(final String projectName, final String currentUserId) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (projectRepository.findOne(projectName, currentUserId) == null) return false;
        final String projectId = projectRepository.findOne(projectName, currentUserId).getId();
        final List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskIdList != null) {
            for (final String taskId : taskIdList) {
                if (taskId == null || taskId.isEmpty()) continue;
                taskRepository.removeById(taskId); //вместе с удалением проекта удаляем все задачи проекта
            }
        }
        return projectRepository.remove(projectName, currentUserId);
    }

    public void removeAll() {
        final List<String> allTaskIdList = taskOwnerRepository.findAll();
        for (final String taskId : allTaskIdList) {
            taskRepository.removeById(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll();
    }

    public void removeAll(final String currentUserId) {
        if (currentUserId == null || currentUserId.isEmpty()) return;
        final List<String> allTaskIdList = new ArrayList<>();
        final List<Project> projects = projectRepository.findAll(currentUserId);
        for (final Project project : projects) {
            if (taskOwnerRepository.findAllByProjectId(project.getId()) == null) continue;
            allTaskIdList.addAll(taskOwnerRepository.findAllByProjectId(project.getId()));
        }
        for (final String taskId : allTaskIdList) {
            taskRepository.removeById(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll(currentUserId);
    }

    public boolean setTask(final String projectName, final String taskName) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectRepository.findOne(projectName) == null) return false;
        if (taskRepository.findOne(taskName) == null) return false;
        final String projectId = projectRepository.findOne(projectName).getId();
        final String taskId = taskRepository.findOne(taskName).getId();
        taskOwnerRepository.merge(projectId, taskId);
        return true;
    }

    public boolean setTask(final String projectName, final String taskName, final String currentUserId) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (taskName == null || taskName.isEmpty()) return false;
        if (currentUserId == null || currentUserId.isEmpty()) return false;
        if (projectRepository.findOne(projectName, currentUserId) == null) return false;
        if (taskRepository.findOne(taskName, currentUserId) == null) return false;
        final String projectId = projectRepository.findOne(projectName, currentUserId).getId();
        final String taskId = taskRepository.findOne(taskName, currentUserId).getId();
        taskOwnerRepository.merge(projectId, taskId);
        return true;
    }

    public List<String> tasksList(final String projectName) { //возвращает список задач проекта
        if (projectName == null || projectName.isEmpty()) return null;
        if (projectRepository.findOne(projectName) == null) return null;
        final String projectId = projectRepository.findOne(projectName).getId();
        if (taskOwnerRepository.findAllByProjectId(projectId) == null) return null;
        final List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskRepository.findAllById(taskIdList) == null) return null;
        final List<Task> taskList = taskRepository.findAllById(taskIdList);
        final List<String> taskNameList = new ArrayList<>();
        for (final Task task : taskList) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

    public List<String> tasksList(final String projectName, final String currentUserId) { //возвращает список задач проекта
        if (projectName == null || projectName.isEmpty()) return null;
        if (currentUserId == null || currentUserId.isEmpty()) return null;
        if (projectRepository.findOne(projectName, currentUserId) == null) return null;
        final String projectId = projectRepository.findOne(projectName, currentUserId).getId();
        if (taskOwnerRepository.findAllByProjectId(projectId) == null) return null;
        final List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskRepository.findAllById(taskIdList) == null) return null;
        final List<Task> taskList = taskRepository.findAllById(taskIdList);
        final List<String> taskNameList = new ArrayList<>();
        for (final Task task : taskList) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

}
