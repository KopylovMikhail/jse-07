package ru.kopylov.tm.service;

import ru.kopylov.tm.api.service.ITerminalService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class TerminalService implements ITerminalService {

    private final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Override
    public String getReadLine() throws IOException {
        return reader.readLine();
    }
}
