package ru.kopylov.tm;

import ru.kopylov.tm.context.Bootstrap;

/**
 * @author Mikhail Kopylov
 * task/project manager
 */

public final class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }

}
