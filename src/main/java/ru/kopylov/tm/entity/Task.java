package ru.kopylov.tm.entity;

import java.util.Date;
import java.util.UUID;

public final class Task extends AbstractEntity {

    private String id = UUID.randomUUID().toString();

    private String name;

    private String description;

    private Date dateStart;

    private Date dateFinish;

    private String userId;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(final Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateFinish() {
        return dateFinish;
    }

    public void setDateFinish(final Date dateFinish) {
        this.dateFinish = dateFinish;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

}
