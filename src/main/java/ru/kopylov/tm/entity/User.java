package ru.kopylov.tm.entity;

import ru.kopylov.tm.enumerated.TypeRole;

import java.util.UUID;

public final class User extends AbstractEntity {

    private String id = UUID.randomUUID().toString();

    private String login;

    private String password;

    private TypeRole role;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public TypeRole getRole() {
        return role;
    }

    public void setRole(final TypeRole role) {
        this.role = role;
    }

}
